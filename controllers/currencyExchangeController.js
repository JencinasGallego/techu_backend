require('dotenv').config();

const requestJson = require('request-json');
const baseCurrencyLayer ="http://apilayer.net/api/";
const currencyLayerApiKey = "access_key=" + process.env.CURRECYLAYER_API_KEY;
const crypt = require('../crypt');


function getCurrencyRateV1(req,res){
  console.log('GET /apibank/v2/currencyrate');
  var httpClient = requestJson.createClient(baseCurrencyLayer);
  console.log('client created');
  var query = '&source=USD&currencies=EUR,GBP&format=1';
  httpClient.get("live?" + currencyLayerApiKey +query ,
    function(err,resMlab,body){
      if(err){
        res.status(500).send({"msg":"Ocurrio un error"});
      }else{
        res.status(200).send(body.quotes);
      }
    }
  )

}





module.exports.getCurrencyRateV1 = getCurrencyRateV1;
