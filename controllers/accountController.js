require('dotenv').config();


//const io = require('../io');
const crypt = require('../crypt');

const requestJson = require('request-json');
const baseMlabURL ="https://api.mlab.com/api/1/databases/apitechujeg9ed/collections/";
const mLabAPIKey ="apiKey=" + process.env.MLAB_API_KEY;

function getAccountByUserIdV1(req,res) {

  console.log('GET /apitechu/v1/accounts/:id');
  var httpClient = requestJson.createClient(baseMlabURL);

  console.log("client created");

  var userId = req.params.id;
  console.log('UserId solicitado:' + userId);
  var query ='q={"UserId":"'+ userId +'"}';
  console.log(query);

  httpClient.get("account?" + query + "&" + mLabAPIKey,
    function(err,resMLab,body){
      //console.log(resMLab);
      //console.log("body: "+body);
      if(err){
        var response = {
          "msg" : "Error obteniendo el usuario"
        }
        res.status(500);
      }else{
        if(body.length>0){
          //var response = body[0];
            var response = body;
        }else{
          var response = {
            "msg" : "usuario sin cuenta asociada"
          }
          res.status(404);
        }
      }

      res.send(response);
    }
  );
}

function getAccountsV1(req,res) {
  console.log('GET /apitechu/v1/accounts');
  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("client created");

  httpClient.post("account?" +"&"+ mLabAPIKey,
    function(err,resMLab,body){
      //console.log(resMLab);
      //console.log("body: "+body);
      var response = !err ? body : {
        "msg" : "Error obteniendo usuarios"
      }

      res.send(response);
    }
  );
}

function createAccountV1(req,res){
  console.log('POST /apibank/v1/accounts/:id');
  console.log(req.params.id);
  var httpClient = requestJson.createClient(baseMlabURL)
  var query = "q={'available':true}&l=1";
  console.log(query);
  httpClient.get("iban?"+query+"&"+mLabAPIKey,
  function(errGetIban,resGetIban,bodyGetIban){
    console.log(bodyGetIban);
    if(errGetIban){
      res.status(500).send({"msg":"Los servicios no estan disponibles, Intentelo más tarde" });
    }else{
      if(bodyGetIban.length==0){
        res.status(400).send({"msg":"No hay IBAN disponibles"})
      }else{
        var newAccount = {
          "IBAN":bodyGetIban[0].iban,
          "UserId":req.params.id,
          "Balance":0
        }
        console.log(newAccount);
        httpClient.post("account?"+ mLabAPIKey,newAccount,
          function(errPacc,resPacc,bodyPacc){
            console.log(bodyPacc);
            if(errPacc){
              res.status(500).send({"msg":"Los servicios no estan disponibles, Intentelo más tarde"});
            }else{
              var queryPut = "q={'iban':'" + bodyGetIban[0].iban + "'}";
              var putBody = '{"$set":{"available":"false"}}';
              httpClient.put("iban?"
              + queryPut
              +"&"
              + mLabAPIKey, JSON.parse(putBody),
              function(errPut, resPut, bodyPut){
                if(errPut){
                  res.status(500).send({"msg":"Los servicios no estan disponibles, Intentelo más tarde"});
                }else{
                  console.log('Iban asignado a nuevo ususario');
                  var response = {"msg": "Cuenta corriente creada con exito"};
                  console.log(response);
                  res.status(201).send(response);
                }
              }
              )
            }
          }
        )
      }
    }
  }
)







}

module.exports.getAccountByUserIdV1 = getAccountByUserIdV1;
module.exports.getAccountsV1 = getAccountsV1;
module.exports.createAccountV1 = createAccountV1;
