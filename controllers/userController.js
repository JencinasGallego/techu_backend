require('dotenv').config();

const requestJson = require('request-json');
//const baseMlabUrl ="https://api.mlab.com/api/1/databases/apitechujeg9ed/collections/";
const baseMlabUrl ="https://api.mlab.com/api/1/databases/apitechujeg9ed/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const crypt = require('../crypt');



function getUsersV1(req,res){
  console.log('GET/apibank/v1/users');

  var users = require('../usuarios.json');
  res.send(users);

}

function getUsersV2(req,res){
  console.log('GET /apibank/v2/users');
  var httpClient = requestJson.createClient(baseMlabUrl);
  console.log('client created');

  httpClient.get("user?" + mLabAPIKey,
    function(err,resMlab,body){
      console.log('-------resMalab--------');
      console.log(resMlab);
      console.log('-------body--------');
      console.log(body);
      var response = !err ? body : {
        "msg" : "Error obteniendo usuarios"
      }
      res.send(response);
    }
  )

}

function getUsersByIdV1(req,res){
  console.log('GET /apibank/v2/users/:id');

  var httpClient = requestJson.createClient(baseMlabUrl);
  console.log('Client created');

  var id =req.params.id;
  console.log('El id solicitado: ' + id);
  var query ='q={"id":' + id + '}';
  console.log(query);

  httpClient.get("collections/user?" + query + "&" + mLabAPIKey,
    function(err, resMlab, body){
      var status;
      var response = !err ? body[0] : (
        status = 500,
        {
          "msg" : "Error obteniendo los datos"
        }
      )
      if(status!=500){
        if(body.length>0){
          status = 200;
        }
        else {
          status = 404;
          response = {
            "msg" : "Error obteniendo los datos"
          }
        }
      }
      res.status(status).send(response);
    }
  )

}

function createUsersV1(req, res){


      console.log("POST /apitechu/v2/users");
      console.log(req.body.username);
      console.log(req.body.lastname1);
      console.log(req.body.lastname2);
      console.log(req.body.email);
      console.log(req.body.password);

      //create httpClient
      var httpClient = requestJson.createClient(baseMlabUrl);
      console.log(baseMlabUrl);
      console.log('Cliente creado');

      //Check if user exist in user collection
      var query ='q={"email":"' + req.body.email + '"}';
      console.log(query);
      httpClient.get("collections/user?" + query + "&" + mLabAPIKey,
        function(errget, resget, bodyget){
          console.log(bodyget)
          if(!errget && bodyget.length==0){
            // Create new user object
            var newUsers ={
              "id":"",
              "username": req.body.username,
              "lastname1": req.body.lastname1,
              "lastname2": req.body.lastname2,
              "email":req.body.email,
              "password":crypt.hash(req.body.password)
            }
            // Generate sequencial Id n

            var data = {
              "findAndModify" : "uniqueId",
              "query" : { "KEY": "ID_USERS" },
              "update" : {
               "$inc": { "COUNT": 1 }
             }
            }
            console.log(data);

            httpClient.post("runCommand?" + mLabAPIKey,data,
              function(errp1,resp1,bodyp1){
                console.log(bodyp1.value.COUNT);
                if(errp1){

                }else {
                    newUsers.id = bodyp1.value.COUNT + 1;
                  }
                  console.log(newUsers);
                  httpClient.post("collections/user?" + mLabAPIKey, newUsers,
                    function(errp2,resp2,bodyp2){
                      //console.log(resMLab);
                      console.log("User saved ok");
                      res.status(201).send(
                        {
                          "msg": "Usuario guardado con éxito",
                          "id":newUsers.id
                        }
                      );
                    }
                  )
                }
              )
          }else{
            if(bodyget[0]){
              console.log("El usuario ya existe");
              res.status(400).send({"msg" : "El usuario existe"});
            }else{
              console.log("Error al consultar si el usuario existe");
              res.status(500).send({"msg" : "Ocurrio un error en el registro"});
            }
          }
        }
      )



      }


// function userUniqueId(){
//   console.log('useruniqueId');
//   var httpClient = requestJson.createClient("https://api.mlab.com/api/1/databases/apitechujeg9ed/");
//   console.log('Cliente creado');
//   var data = {
//     "findAndModify" : "uniqueId",
//     "query" : { "KEY": "ID_USERS" },
//     "update" : {
//      "$inc": { "COUNT": 1 }
//    }
//   }
//   console.log(data);
//
//   var response={};
//   httpClient.post("runCommand?" + mLabAPIKey,data,
//     function(err,res,body){
//       //console.log(body);
//       //console.log(res);
//       console.log("---PRUEBAS----");
//       //console.log(body.lastErrorObject.updatedExisting);
//       console.log(body.ok);
//       console.log(body.value.COUNT);
//       if(err){
//         response={
//           id:0,
//           status:500
//         }
//       }else {
//         console.log('else');
//           var newId = body.value.COUNT + 1;
//            response={
//             id:newId,
//             status:200
//           }
//         }
//         console.log(response);
//         console.log("fin del runCommand");
//         return response
//       }
//   )
// }
//
// function test(req,res){
//   var prueba = userUniqueId();
//   console.log(prueba);
//   console.log("fin");
// }





module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUsersByIdV1 = getUsersByIdV1;
module.exports.createUsersV1 = createUsersV1;



// Solo metodos de Tests
//module.exports.test=test;
