const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujeg9ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function createLoginV1(req, res){
  console.log("POST /apibank/v1/login");
  console.log("Email: " + req.body.email);
  console.log("Password: " + req.body.password);

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log('client created');

  var email = req.body.email;
  var password =req.body.password;
  var query ='q={"email": "' + email + '"}';
  console.log(query);
  console.log(baseMlabURL+'user?' + query + '&' + mLabAPIKey);
  httpClient.get('user?' + query + '&' + mLabAPIKey,
    function(err, resMlab, body){
      console.log(body);
      //console.log(resMlab)
      if(err){
        console.log('error 500')
        res.status(500).send({"msg":"Login incorrecto"});
      }else {
        if(body.length>0){

          var passwordHash = body[0].password;
          console.log(passwordHash);
          console.log("check pass: " + password + "||" + passwordHash);

          var found = crypt.checkPassword(password,passwordHash);
          console.log("User founded? " + found);
        }else {
          console.log("usuario no encontrado")
          var found = false;
          res.status(401).send({"msg" : "Login Incorrecto"});
        }
      }
      console.log(found);
      if(found){
        var putBody = '{"$set":{"logged":"true"}}';
        httpClient.put(
          "user?"
          + query
          + "&"
          + mLabAPIKey, JSON.parse(putBody), function(errPUT, resPUT, bodyPUT){
            console.log(bodyPUT);

            if(err){
              res.status(500).send({"msg":"Login incorrecto"});
            }else {
              var response = {
                "msg":"Login correcto",
                "id" : body[0].id
              }
              console.log(response);
              res.status(200).send(response);
            }
          }
        )
      }
    }
  )
}

function createLogOutV1(req,res){
  console.log("POST /apibank/v1/logout/:id");
  console.log(req.params.id);
  var id = req.params.id;
  var query ='q={"id":'+ id +'}';
  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("client created");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
  function(err,resMLab,body){
    console.log(body);
    if(err){
      res.status(500).send({"msg":"Error obteniendo el usuario"});
    }else{
      if(body.length>0){
        var logged = body[0].logged;
      }else{
        res.status(401).send({"msg" : "Error obteniendo el usuario"});
      }
  }
      if(logged){
        var putbody='{"$unset":{"logged":""}}';
        httpClient.put(
          "user?"
          + query
          + "&"
          + mLabAPIKey, JSON.parse(putbody), function(errPUT, resMLabPUT, bodyPUT){
            console.log(bodyPUT);
            if(err){
              res.status(500).send({"msg" : "Error obteniendo el usuario"});
            }else{
              console.log("LogOut Correcto");
              var response={
                "msg":"LogOut correcto",
                "id":id
              };
              res.status(200).send(response);
          }
        }
      )
    }else{
      console.log("El LogOut no se pudo realizar")
      var response={
        "msg":"El logOut no se puede realizar",
        "id":id
      };
      res.status(400).send(response);
    }
  }
)




}




module.exports.createLoginV1 = createLoginV1;
module.exports.createLogOutV1 = createLogOutV1;
