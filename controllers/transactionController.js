require('dotenv').config();
//const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMlabURL ="https://api.mlab.com/api/1/databases/apitechujeg9ed/collections/";
const mLabAPIKey ="apiKey=" + process.env.MLAB_API_KEY;

function createTransactionV1(req, res){
  console.log("POST /apibank/v1/transactions");
  console.log(req.body);

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("client created");
  httpClient.post("transactions?"+mLabAPIKey,req.body,
    function(errP,resP,bodyP){
      if(errP){
        res.status(500).send({"msg":"Ocurrio un error durante la operación"});
      }else {
        console.log("transaction created");
        var query = "q={'IBAN':'"+req.body.iban+"'}";
        console.log(query);
        httpClient.get("account?"+query+"&"+mLabAPIKey,
          function(errG,resG,bodyG){
            console.log(bodyG);
            if(errG){
              res.status(500).send({"msg":"Ocurrio un error durante la operación"});
            }else{
              var saldoActual = bodyG[0].Balance;
              var importeRecibido = parseInt(req.body.importe, 10);
              if(req.body.tipo_op=='Ingreso'){
                var saldoNuevo = saldoActual + importeRecibido;
              }else{
                var saldoNuevo = saldoActual - importeRecibido;
              }
              console.log(saldoNuevo);
              var putBody = "{\"$set\":{\"Balance\":" + saldoNuevo + "}}";
              httpClient.put("account?"
                + query
                +"&"
                + mLabAPIKey,JSON.parse(putBody),
                function(errPut,resPut,bodyPut){
                  if(errPut){
                    res.status(500).send({"msg":"Ocurrio un error durante la operación"});
                  }else {
                    console.log(bodyPut);
                    var response = {
                      "msg":"Balance actualizado",
                      "Balance":saldoNuevo
                    }
                    res.status(201).send(response);
                  }
                }
              )
            }
          }
        )
      }
    }
  )
}

function getTransactionsByIBANV1(req,res){
  console.log('/apibank/v1/getTransactions/');
  //console.log(req.body.iban);
  console.log(req.headers);


  var httpClient = requestJson.createClient(baseMlabURL);
  var query = "q={'iban':'" +req.headers.iban + "'}";
  httpClient.get("transactions?"+ query +"&"+ mLabAPIKey,
    function(errGet, resGet, bodyGet){
      console.log(bodyGet);
      if(errGet){
        res.status(500).send({"msg":"error obteniendo los datos"});
      }
      if(bodyGet.length==0){
        res.status(404).send({"msg":"no existen transacciones"});
      }else{
        console.log('ejecución 200');
        res.status(200).send(bodyGet);
      }
    }
  )
}


module.exports.createTransactionV1 = createTransactionV1;
module.exports.getTransactionsByIBANV1 = getTransactionsByIBANV1;
