const bcrypt = require('bcrypt');

function hash(data){
  console.log("Hashing data");
  return bcrypt.hashSync(data,10);
}

function checkPassword(passwordFromUserinPlainText, passwordFromDBHashed){
  console.log("Checking password");
  return bcrypt.compareSync(passwordFromUserinPlainText, passwordFromDBHashed);
}

module.exports.hash = hash;
module.exports.checkPassword = checkPassword; 
