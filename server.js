

const express = require('express');
const app = express();
const userController = require('./controllers/userController');
const authController = require('./controllers/authController');
const accountController = require('./controllers/accountController');
const transactionController = require('./controllers/transactionController');
const currencyExchangeController = require('./controllers/currencyExchangeController');

var enableCORS = function(req, res, next) {

 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");

 res.set("Access-Control-Allow-Headers", "*");
 next();

}



app.use(express.json());
app.use(enableCORS);

const port = process.env.PORT || 3000;
app.listen(port);
console.log("API escuchando en el puerto: "+port);

app.get('/apibank/v1/users',userController.getUsersV1);
app.get('/apibank/v2/users',userController.getUsersV2);
app.get('/apibank/v1/users/:id',userController.getUsersByIdV1);
app.get('/apibank/v1/accounts', accountController.getAccountsV1);
app.get('/apibank/v1/accounts/:id', accountController.getAccountByUserIdV1);
app.get('/apibank/v1/transactions/',transactionController.getTransactionsByIBANV1);
app.get('/apibank/v1/currencyExchange',currencyExchangeController.getCurrencyRateV1);




app.post('/apibank/v1/users',userController.createUsersV1);
app.post('/apibank/v1/login',authController.createLoginV1);
app.post('/apibank/v1/logout/:id',authController.createLogOutV1);
app.post('/apibank/v1/accounts/:id',accountController.createAccountV1);
app.post('/apibank/v1/transactions',transactionController.createTransactionV1);


// Solo llamadas de test
//app.post('/apitest/idInc', userController.test);
